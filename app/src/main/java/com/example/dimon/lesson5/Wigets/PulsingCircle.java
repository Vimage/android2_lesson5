package com.example.dimon.lesson5.Wigets;

import android.content.Context;

import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.example.dimon.lesson5.R;


/**
 * TODO: document your custom view class.
 */
public class PulsingCircle extends View {

    /* Private field for store a Paint object */
    private Paint mPaint = null;
    private int color = Color.RED;

    public PulsingCircle(Context context) {
        super(context);
        init(null, 0);
    }

    public PulsingCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        // Obtain style attributes
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PulsingCircle);
        color = a.getColor(R.styleable.PulsingCircle_color, color);
        a.recycle();

        init(attrs, 0);

    }


    private void init(AttributeSet attrs, int defStyle) {
            /* Create a Paint object */
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setStrokeWidth(1);
        //mPaint.setTextSize(mTextSize);
        mPaint.setAntiAlias(true);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        int r;
        if (contentWidth < contentHeight) {
            r = (contentWidth / 2) - 2;
        } else {
            r = (contentHeight / 2) - 2;
        }
        canvas.drawCircle(contentWidth / 2, contentHeight / 2, r, mPaint);
    }




}
