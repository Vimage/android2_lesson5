package com.example.dimon.lesson5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.dimon.lesson5.Wigets.PulsingCircle;

public class MainActivity extends AppCompatActivity {

    PulsingCircle pulsingCircle;
    Animation anim = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pulsingCircle = (PulsingCircle) findViewById(R.id.pulsingCircle);
    }

    public void onButton1Click(View view) {

        anim = AnimationUtils.loadAnimation(this, R.anim.size);

        pulsingCircle.startAnimation(anim);
    }

    public void onButton2Click(View view) {

        anim = AnimationUtils.loadAnimation(this, R.anim.alpha);

        pulsingCircle.startAnimation(anim);
    }

    public void onButton3Click(View view) {

        anim = AnimationUtils.loadAnimation(this, R.anim.anim_set);

        pulsingCircle.startAnimation(anim);
    }
}
